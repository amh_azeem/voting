from datetime import datetime

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from django.shortcuts import HttpResponse, redirect, get_object_or_404
# Create your views here.
from django.template.response import TemplateResponse
from random import randrange

from poll.form import PositionForm, NomineeForm, SettingForm
from poll.models import Position, Nominee, Settings, Department


def alert(message):
    return {'alert_message':message}

def show_error404(request,message):
    return TemplateResponse(request,'poll/super/error_404.html',{'error_message':message})

def get_permissible_departments(request):
    user=get_object_or_404(User,username=request.user.username)
    departments = Department.objects.all().filter(user=user)
    return departments.values_list()


def get_expiry_date(department):
    department = department.upper()
    try:
        dept=Department.objects.get(name=department)
        return {'EXPIRY_DATE': dept.end_date, 'START_DATE': dept.start_date,'DEPARTMENT':dept}
    except Department.DoesNotExist:
        dept = Department.objects.get(name="DEFAULT")
        return {'EXPIRY_DATE': dept.end_date, 'START_DATE': dept.start_date,'DEPARTMENT':dept}

def update_ctx(ctx,department):
    department=department.upper()
    if ctx:
        ctx.update(get_expiry_date(department))
    else:
        ctx = get_expiry_date(department)
    return ctx

def index(request,department="default"):
    if department=='ascon':
        d=Department.objects.get(name='ASCON')
        positions=Position.objects.all().filter(department=d)
        ctx={'positions':positions,'home':'1'}
        update_ctx(ctx,'ascon')
        return TemplateResponse(request, "poll/default/default.html",ctx)
    return TemplateResponse(request, "poll/default/default.html", get_expiry_date(department))

    # return TemplateResponse(request, "poll/{}/index.html".format(department),get_expiry_date(department))

def position_index(request,department="default"):
    department = department.upper()
    try:
        position_list = Department.objects.get(name=department.upper())
        ctx = {"positions": position_list}
        if ctx:
            ctx.update(get_expiry_date(department))
        else:
            ctx = get_expiry_date(department)
        return TemplateResponse(request, "poll/default/default-position-index.html", ctx)
        # return TemplateResponse(request, "poll/{}/position-index.html".format(department), ctx)
    except Position.DoesNotExist:
        return HttpResponse("Page Not found")

def position_detail(request,department, position_id):
    try:
        position = Position.objects.get(pk=position_id)
        ctx = {"position": position}
        return TemplateResponse(request, "poll/default/default-positiion-detail.html", ctx)
    except Position.DoesNotExist:
        return HttpResponse("Doesn't Exist")

def vote(request,department, position_id):
    error_message = ""
    if request.method == "POST":
        if request.session.get('has_voted_{}'.format(position_id), 0):
            return TemplateResponse(request, "poll/default/error_voting.html", get_expiry_date())
            # return TemplateResponse(request, "poll/{}/error_voting.html".format(department),get_expiry_date())
        choice = request.POST["choice"]
        dept=Department.objects.get(name=department.upper())
        # request.session.set_expiry(int(os.environ.get('EXPIRY_SECONDS', 7200)))
        request.session.set_expiry(int(dept.vote_timeout)*3600)
        try:
            if not request.user.is_authenticated:
                request.session['has_voted_{}'.format(position_id)] = 1
                val = request.session.get_expiry_date().isoformat()
                request.session['expires_value_{}'.format(position_id)] = val
            nominee = Nominee.objects.get(pk=choice)
            nominee.votes += 1
            nominee.save()
            p = Position.objects.get(pk=position_id)
            p.votes_count += 1
            p.save()

        except Nominee.DoesNotExist:
            error_message = "Some error occurred"
        finally:
            if error_message:
                return redirect("poll:position-detail", position_id=position_id,department=department)
            return redirect("poll:successful-vote", position_id=position_id,department=department)
    else:

        if request.session.get('has_voted_{}'.format(position_id), 0):
            expir = request.session.get('expires_value_{}'.format(position_id), datetime.now().isoformat())
            ctx=update_ctx({'expires_at':expir},department)
            return TemplateResponse(request, "poll/default/error_voting.html", ctx)
            # return TemplateResponse(request, "{}/poll/error_voting.html".format(department),ctx)
        _obj = Position.objects.get(pk=position_id)
        ctx={"position": _obj}
        update_ctx(ctx,department)
        return TemplateResponse(request,"poll/default/default-positiion-detail.html",ctx)
        # return TemplateResponse(request, "poll/{}/position-detail.html".format(department), ctx)

def success(request,department, position_id):
    p = get_object_or_404(Position, pk=position_id)
    ctx=  {"position": p}
    update_ctx(ctx,department)
    # return TemplateResponse(request, "poll/{}/success.html".format(department), ctx)
    return TemplateResponse(request, "poll/default/default-success.html", ctx)


@login_required
def super_index(request):
    user=get_object_or_404(User,username=request.user.username)
    departments = Department.objects.all().filter(user=user)

    return TemplateResponse(request,'poll/super/super_index.html',{'departments':departments})

@login_required
def super_position_index(request,department_id='-1'):
    if department_id== "-1":
        return redirect('poll:super-index')
    try:
        department=get_object_or_404(Department,pk=department_id)
        # positions=Position.objects.all().filter(department=department)
        return TemplateResponse(request, 'poll/position/position_index.html', {'department': department})
    except Position.DoesNotExist:
        msg="Position does not exist"
        show_error404(request,msg)

@login_required
def super_position_add(request,department_id):
    form=PositionForm(request.POST or None)
    try:
        department=Department.objects.get(pk=department_id)
        if request.method == "POST":
            pos=Position()
            form=PositionForm(request.POST,instance=pos)
            if form.is_valid():
                new_form=form.save(commit=False)
                new_form.department = department
                new_form.save()

                return redirect('poll:super-position-index',department_id=department_id)
            else:
                return TemplateResponse(request, 'poll/position/position_add.html',
                                        {'form': form, 'department': department})
        else:
            return TemplateResponse(request, 'poll/position/position_add.html',
                                    {'form': form, 'department': department})
    except Department.DoesNotExist:
        msg='Department does not exist'
        show_error404(request,msg)


@login_required
def super_position_detail(request,position_id):
    return redirect("poll:super-position-edit",position_id=position_id)

@login_required
def super_position_delete(request,position_id):
    try:
        position=Position.objects.get(pk=position_id)
    except Position.DoesNotExist:
        return show_error(request, "Sorry, Position not found")
    if request.method=="POST":
        position.delete()
        return redirect("poll:super-position-index")
    else:
        return TemplateResponse(request, 'poll/position/position_delete.html', {'position':position})

@login_required
def super_position_edit(request,position_id):
    try:
        position=Position.objects.get(pk=position_id)
        da= position.department_id
        d=Department.objects.get(pk=da)
        form = PositionForm(request.POST or None, instance=position)
        if request.method == "POST":
            if form.is_valid():
                _f = form.save(commit=False)
                _f.department = d
                _f.save()
                return redirect('poll:super-position-index')
            else:
                return TemplateResponse(request, 'poll/position/position_edit.html', {'form': form, 'pk': position_id})
        else:
            return TemplateResponse(request, 'poll/position/position_edit.html', {'form': form, 'pk': position_id})

    except Position.DoesNotExist:
        return show_error(request, "Sorry, Position not found")

@login_required
def super_nominee_index2(request):
    u=get_object_or_404(User,username=request.user.username)
    d=Department.objects.get(user=u)
    p=d.position_set.all()
    return TemplateResponse(request, 'poll/nominee/nominee_index2.html', {'nominees':Nominee.objects.all(),'positions':p})

@login_required
def super_nominee_index(request,position_id):
    try:
        position=Position.objects.get(pk=position_id)
    except Position.DoesNotExist:
        return show_error(request.POST, "Sorry, Position not found")
    return TemplateResponse(request, 'poll/nominee/nominee_index.html', {'position':position})

@login_required
def show_error(request,message):
    return TemplateResponse(request, 'poll/super/error_404.html', {'error_message':message})

@login_required
def super_nominee_add(request,position_id):
    try:
        position=Position.objects.get(pk=position_id)
        form = NomineeForm(request.POST or None, request.FILES or None)

        if request.method == "POST":
            if form.is_valid():
                _f = form.save(commit=False)
                _f.position = position
                _f.save()
                return redirect('poll:super-nominee-index', position_id=position_id)
            else:
                return TemplateResponse(request, 'poll/nominee/nominee_add.html', {'form': form,'position':position})
        else:
            return TemplateResponse(request, 'poll/nominee/nominee_add.html', {'form': form,'pk':position_id,'position':position})
    except Position.DoesNotExist:
        return show_error("Sorry, that position doesn't exist")

@login_required
def super_nominee_add2(request):
    form=NomineeForm(request.POST or None, request.FILES or None)
    if request.method=="POST":
        if form.is_valid():
            form.save()
            return redirect('poll:super-nominee-index')
        else:
            return TemplateResponse(request, 'poll/nominee/nominee_add2.html', {'form':form})
    else:
        return TemplateResponse(request, 'poll/nominee/nominee_add2.html', {'form':form})

@login_required
def super_nominee_detail(request, position_id, nominee_id):
    return redirect("poll:super-nominee-edit",position_id=position_id,nominee_id=nominee_id)

@login_required
def super_nominee_edit(request,position_id,nominee_id):
    try:
        nominee=Nominee.objects.get(pk=nominee_id)
    except Nominee.DoesNotExist:
        return show_error(request, "Sorry, Nominee not found")
    form=NomineeForm(request.POST or None, request.FILES or None, instance=nominee)
    if request.method=="POST":
        if form.is_valid():
            form.save()
            return redirect('poll:super-nominee-index')
        else:
            return TemplateResponse(request, 'poll/nominee/nominee_edit.html', {'form':form, 'nominee':nominee})
    else:
        return TemplateResponse(request, 'poll/nominee/nominee_edit.html', {'form':form, 'nominee':nominee})


@login_required
def super_nominee_delete(request,nominee_id):
    try:
        nominee=Nominee.objects.get(pk=nominee_id)
    except Nominee.DoesNotExist:
        return show_error(request, "Sorry, Nominee not found")
    if request.method=="POST":
        nominee.delete()
        return redirect("poll:super-nominee-index")
    else:
        return TemplateResponse(request, 'poll/nominee/nominee_delete.html', {'nominee':nominee})

@login_required
def result_index(request):
    u=get_object_or_404(User,username=request.user.username)
    d=Department.objects.get(user=u)
    p=d.position_set.all().order_by('title')
    return TemplateResponse(request, 'poll/super/result_index.html', {'positions':p})

@login_required
def result_list(request):
    u=get_object_or_404(User,username=request.user.username)
    d=Department.objects.get(user=u)
    p=d.position_set.all().order_by('title')
    return TemplateResponse(request, 'poll/super/result_list.html', {'positions':p})

@login_required
def result_detail(request, position_id):
    u=get_object_or_404(User,username=request.user.username)
    d=Department.objects.get(user=u)
    positions=d.position_set.all().order_by('title')
    data = []
    user_data = []
    color_data = []
    position = Position.objects.get(pk=position_id)
    for a in position.nominee_set.all().order_by('-votes'):
        data.append(a.votes)
        user_data.append(a.get_fullname())
        color_data.append(
            "rgb(" + str(randrange(11, 255)) + ", " + str(randrange(40, 100)) + "," + str(randrange(0, 200)) + ")")
    winner = position.get_winner()
    return TemplateResponse(request, 'poll/super/result_detail.html',
                            {'position': position, 'user_data': user_data, 'data': data,
                             'colors': color_data, 'positions': positions, 'winner': winner})


def login_user(request):
    error_message = ""
    if request.user.is_authenticated:
        return redirect("poll:super-index")
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        try:
            user = User.objects.get(username=username)
            if not user.is_active:
                error_message="Sorry, account disabled, contact the administrator"
            if not error_message:
                if check_password(password, user.password):
                    a=authenticate(username=username,password=password)
                    login(request,user)

                else:
                    error_message = "Invalid Password"
        except User.DoesNotExist:
            error_message = "Sorry, No account exists with the username"

        finally:
            if error_message:
                return TemplateResponse(request, "poll/super/super_login.html", {"error_message": error_message})
            return redirect("poll:super-index")
    else:
        return TemplateResponse(request, "poll/super/super_login.html", {"error_message": error_message})


def logout_user(request):
    logout(request)
    return redirect("poll:super-login")


def my_settings(request):
    _f=Settings.objects.get(pk=1)
    form=SettingForm(request.POST or None)
    if request.method=='POST':
        if form.is_valid():
            form.save()
            return redirect("poll:super-index")

    else:
        return TemplateResponse(request, 'poll/super/super_settings.html', {'form':form})


def super_department_index(request):

    return None