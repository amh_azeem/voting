from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Department(models.Model):
    name=models.CharField(max_length=55)
    event_title=models.CharField(max_length=200,blank=True,null=True)
    description=models.CharField(max_length=200,blank=True,null=True)
    image=models.ImageField(blank=True,null=True)
    start_date=models.DateTimeField(blank=True,null=True)
    end_date=models.DateTimeField(blank=True,null=True)
    vote_timeout=models.IntegerField(verbose_name="Vote timeout in hours",default=2)
    user=models.ManyToManyField(User)
    def __str__(self):
        return self.name

    def get_total_positions(self):

        return self.position_set.count()
    def get_total_nominees(self):
        sum=0
        return


class Position(models.Model):
    title=models.CharField(max_length=200)
    department=models.ForeignKey(Department, on_delete=models.CASCADE,blank=True,null=True)
    votes_count=models.IntegerField(default=0,blank=True,null=True)
    def __str__(self):

        return self.title + "[ "+self.department.__str__()+" ]"
    def get_winner(self):
        a=self.nominee_set.all().order_by('-votes').first()
        return a.get_fullname()


class Nominee(models.Model):
    position=models.ForeignKey(Position,on_delete=models.CASCADE,blank=True,null=True)
    first_name=models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    image=models.ImageField(null=True,blank=True)
    votes=models.PositiveIntegerField(default=0)
    def __str__(self):
        return self.get_fullname() + " going for " + self.position.title
    def get_fullname(self):
        return self.first_name + " " + self.last_name

class Settings(models.Model):
    start_date=models.DateTimeField(null=True)
    end_date=models.DateTimeField(null=True)





