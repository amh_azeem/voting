from django.conf.urls.static import static
from django.urls import path

from datadiet import settings
from poll import views

app_name = "poll"
urlpatterns = [
    path('', views.index, name="index"),
    path('<slug:department>', views.index, name="index"),
    path('<slug:department>/positions/', views.position_index, name="position-index"),
    path('<slug:department>/positions/<int:position_id>', views.position_detail, name="position-detail"),
    path('<slug:department>/positions/<int:position_id>/vote', views.vote, name="cast-vote"),
    path('<slug:department>/positions/<int:position_id>/vote/success', views.success, name="successful-vote"),
    path('results/', views.result_index, name="super-result-index"),
    path('results/list', views.result_list, name="super-result-list"),
    path('results/<int:position_id>', views.result_detail, name="result-detail"),
    path('super/', views.super_index, name="super-index"),
    path('super/login', views.login_user, name="super-login"),
    path('super/settings', views.my_settings, name="super-settings"),
    path('super/logout', views.logout_user, name="super-logout"),
    # path('super/department', views.super_department_index, name="super-position-index"),
    path('super/position', views.super_position_index, name="super-position-index"),
    path('super/<int:department_id>/position', views.super_position_index, name="super-position-index"),
    path('super/<int:department_id>/position/add', views.super_position_add, name="super-position-add"),
    path('super/position/<int:position_id>', views.super_position_detail, name="super-position-detail"),
    path('super/position/<int:position_id>/edit', views.super_position_edit, name="super-position-edit"),
    path('super/position/<int:position_id>/delete', views.super_position_delete, name="super-position-delete"),
    path('super/nominee', views.super_nominee_index2, name="super-nominee-index"),
    path('super/position/<int:position_id>/nominee', views.super_nominee_index, name="super-nominee-index"),
    path('super/position/<int:position_id>/nominee/add', views.super_nominee_add, name="super-nominee-add"),
    path('super/position/nominee/add', views.super_nominee_add2, name="super-nominee-add"),
    path('super/position/<int:position_id>/nominee/<int:nominee_id>', views.super_nominee_detail,
         name="super-nominee-detail"),
    path('super/position/<int:position_id>/nominee/<int:nominee_id>/edit', views.super_nominee_edit,
         name="super-nominee-edit"),
    path('super/nominee/<int:nominee_id>/delete', views.super_nominee_delete, name="super-nominee-delete"),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
