from django import forms

from poll.models import Position, Nominee, Settings, Department


class PositionForm(forms.ModelForm):
    class Meta:
        model = Position
        exclude = []
        widgets = {
            'title': forms.TextInput(attrs={'class': 'uk-input'}),
            # 'department': forms.Select(attrs={'class': 'uk-select'}),
        }

class NomineeForm(forms.ModelForm):
    class Meta:
        model = Nominee
        exclude = ['votes']
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'uk-input'}),
            'last_name': forms.TextInput(attrs={'class': 'uk-input'}),
            'nick_name': forms.TextInput(attrs={'class': 'uk-input'}),
            'position': forms.Select(attrs={'class': 'uk-select'}),
            'image': forms.FileInput(),
        }
class DepartmentForm(forms.ModelForm):
    class Meta:
        model=Department
        exclude=['user']
        widgets={
            'event_title':forms.TextInput(attrs={'class': 'uk-input'}),
            'description':forms.TextInput(attrs={'class': 'uk-input'}),
            'end_date':forms.TextInput(attrs={'class': 'uk-input'}),
            'start_date':forms.TextInput(attrs={'class': 'uk-input'}),
        }



class SettingForm(forms.ModelForm):
    class Meta:
        model = Settings
        exclude = []
        widgets = {
            'start_date': forms.TextInput(attrs={'class':"form_datetime",'readonly':''}),
            'end_date': forms.TextInput(attrs={'class':"form_datetime"}),
        }
