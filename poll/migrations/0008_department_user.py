# Generated by Django 2.0.5 on 2018-06-14 16:19

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('poll', '0007_auto_20180614_1550'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='user',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
