# Generated by Django 2.0.5 on 2018-07-28 01:22

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poll', '0009_auto_20180728_0222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='position',
            name='department',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='poll.Department'),
        ),
    ]
