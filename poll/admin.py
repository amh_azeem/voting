from django.contrib import admin
# Register your models here.

from poll.models import Nominee, Position, Settings, Department

admin.site.register(Position)
admin.site.register(Nominee)
admin.site.register(Settings)
admin.site.register(Department)
# admin.site.register(Permission)