"""
Django settings for datadiet project.

Generated by 'django-admin startproject' using Django 2.0.3.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

from datetime import timedelta, timezone, datetime

import dateutil.parser
import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from decouple import config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY','test_secret')

# SECURITY WARNING: don't run with debug turned on in production!
def get_list(text):
    return [item.strip() for item in text.split(',')]
DEBUG = config('DEBUG',True)

ALLOWED_HOSTS = []
# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    "poll",

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'datadiet.urls'
SESSION_SAVE_EVERY_REQUEST = True
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'datadiet.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

# 'default': dj_database_url.config(
#     default='postgres://postgres:postgres@localhost:5432/datadiet2',
#     conn_max_age=600)}
DATABASES = {

    'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': config('DATABASE_NAME', 'datadiet'),
            'USER': config('DATABASE_USER', 'root'),
            'PASSWORD': config('DATABASE_PASSWORD', ''),
            'HOST': config('DATABASE_HOST', 'localhost'),
            'PORT': config('DATABASE_PORT', '3306'),
    }

    }


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/
ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
def location(f):
    return os.path.join(ROOT_DIR, f)


MEDIA_ROOT=location('media/')
MEDIA_URL = '/media/'

TEMPLATE_DIRS = (os.path.join(BASE_DIR,  'templates'),)

#Amazon s3 config
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID',None)
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY',None)
AWS_STORAGE_BUCKET_NAME = os.environ.get('AWS_STORAGE_BUCKET_NAME',None)
AWS_S3_CUSTOM_DOMAIN = os.environ.get('AWS_S3_CUSTOM_DOMAIN',None)
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = os.environ.get('AWS_LOCATION',None)

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'poll/static'),
]

if AWS_STORAGE_BUCKET_NAME:
    STATIC_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
else:
    STATIC_URL='/static/'
if AWS_STORAGE_BUCKET_NAME:
    STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
if AWS_STORAGE_BUCKET_NAME:
    DEFAULT_FILE_STORAGE = 'poll.storage_backend.MediaStorage'

COUNTDOWN_DATE=config('COUNTDOWN_DATE',str(datetime.now()+timedelta(seconds=10)))

LOGIN_URL='poll:super-login'